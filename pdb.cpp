/*
 *  pdb.cpp
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "skutil.h"
#include "pdb.h"

#define P(EX) cout << #EX << ": " << EX << endl;


using namespace std;


Pdb::Pdb(char* n)
: name(n)
{
  ifstream in(n);
  initAtoms(in);
}
Pdb::Pdb(string n)
: name(n)
{
  ifstream in(name.c_str());
  initAtoms(in);
}
Pdb::Pdb(string n, istream& in)
: name(n)
{
  initAtoms(in);
}
Pdb::Pdb(string n, vector<Atom>& a, int resCount)
: name(n),
  atoms(a),
  residueCount(resCount)
{
}

const Atom& Pdb::operator[](int x) const
{
  require(x >= 0 && (unsigned)x < atoms.size(),
    "Pdb::operator[] out of range");
  return atoms[x];
}

unsigned int Pdb::countResidues() const
{
  if (residueCount < 0)
  {
    unsigned int r = 0;
    int prevres = 0;
    string previnscode = "";
    for (unsigned int i=0; i<atoms.size(); i++)
    {
      if (prevres != atoms[i].ires and previnscode != atoms[i].inscode)
      {
        prevres = atoms[i].ires;
        previnscode = atoms[i].inscode;
        r++;
      }
    }
    ((Pdb)(*this)).residueCount = r;
  }
  return residueCount;
}


void Pdb::initAtoms(istream& in)
{
    require(in, string("Could not read file ") + name + "\n");
    
    residueCount=0;
    
    // read file
    string line;
    int lastres=0;
    string lastinscode = "";
    while(getline(in, line))
    {
        if (line.substr(0,4) == "ATOM")
        {
//             cerr << "Building Atom for line: '" << line << "'\n";
            Atom atm(line);
            if (atm.atom.size()>0 and (atm.atom[0] != 'H'))
            {
                // add ATOM to vector of atoms for the current file
                atoms.push_back(atm);
                if (lastres != atm.ires and lastinscode != atm.inscode)
                {
                  lastres = atm.ires;
                  lastinscode = atm.inscode;
                  atm.resindex = residueCount++;
                }
            }
            else
            {
                cerr << "Skipping atom : \n\t";
                atm.print(cerr);
            }
        }
    }
}


Pdb Pdb::copyByAtomType(std::string atype) const
{
  vector<Atom> newAtoms;
  for(unsigned int i=0; i<atoms.size(); i++)
  {
    if (atype == atoms[i].atom)
    {
      newAtoms.push_back(atoms[i]);
    }
  }
  return Pdb(name, newAtoms, residueCount);
}

void Pdb::splitChains(std::vector<Pdb>& output, vector<string>& chainids) const
{
  require(output.size() == chainids.size(), "Pdb::splitChains : output vectors not of same size!");

  map< string, vector<Atom> > chains;
  for(unsigned int i=0; i<atoms.size(); i++)
  {
    string c = atoms[i].chain;
    chains[c].push_back(atoms[i]);
  }
  for(map< string, vector<Atom> >::iterator it=chains.begin(); it!=chains.end(); it++)
  {
    output.push_back(Pdb(name, it->second));
    chainids.push_back(it->first);
  }
}

void Pdb::sort()
{
  ::sort(atoms.begin(), atoms.end());
}


// Atom::Atom(Atom other)
// {
//     this->x=x;
//     this->y=y;
//     this->z=z;
//     this->iatom=iatom;
//     this->atom=atom;
//     this->ires=ires;
//     this->res=res;
//     this->chain=chain;
//     consensus = cons;
// }
// Atom::Atom(std::string x, std::string y, std::string z, std::string iatom, std::string atom, std::string ires, std::string res, std::string chain)
// {
//     this->x=convertTo<double>(x);
//     this->y=convertTo<double>(y);
//     this->z=convertTo<double>(z);
//     this->iatom=convertTo<int>(iatom);
//     this->atom=trim(atom);
//     this->ires=convertTo<int>(ires);
//     this->res=trim(res);
//     this->chain=trim(chain);
// }
Atom::Atom(const string& line)
: consensus(line)
{
    x = convertTo<double>(line.substr(30,8));
    y = convertTo<double>(line.substr(38,8));
    z = convertTo<double>(line.substr(46,8));
    iatom = convertTo<int>(line.substr( 6,5));
    atom  = trim(line.substr(12,4));
    ires  = convertTo<int>(line.substr(22,4));
    res   = trim(line.substr(17,3));
    chain = trim(line.substr(21,1));
    
    altloc  = trim(line.substr(16,1));
    inscode = trim(line.substr(26,1));
    
    resindex = 0;
}

void Atom::print(ostream& out) const
{
    char buffer[100];
    
    sprintf(buffer, "%8.3f %8.3f %8.3f %5d %-4s %s%4d%s %-3s %s\t", x, y, z, iatom, atom.c_str(), altloc.c_str(), ires, inscode.c_str(), res.c_str(), chain.c_str());
    out << buffer;
    consensus.print(out);
}

int Atom::operator<(const Atom& other) const
{
    if (ires != other.ires)
      return ires < other.ires;
    if (inscode != other.inscode)
      return inscode < other.inscode;
    if (altloc != other.altloc)
      return altloc < other.altloc;
    return atom < other.atom;
}

int Atom::operator>(const Atom& other) const
{
    if (ires != other.ires)
      return ires > other.ires;
    if (inscode != other.inscode)
      return inscode > other.inscode;
    if (altloc != other.altloc)
      return altloc > other.altloc;
    return atom > other.atom;
}


Consensus::Consensus(const std::string& line)
{
    vector<string> tokens = split("\t", line);
    if (tokens.size() == 1)
    {
      // No consensus sequence info found.
      // Use the standard PDB residue identifier as the sole observation.
      //
      residues.push_back(trim(line.substr(17,3)));
      counts.push_back(1);
      totalCount = 1;
      return;
    }
    
    require(tokens.size() == 3, "Error parsing Consensus sequence from PDB ATOM line. tokens.size() != 3");
    
    residues = split(",", tokens[1]);
    tokens = split(",", trim(tokens[2]));
    
    require(tokens.size() == residues.size(), "Error parsing Consensus sequence from PDB ATOM line. residues.size() != counts.size()");
    
    for (unsigned int i=0; i<tokens.size(); i++)
    {
      int c = convertTo<int>(tokens[i]);
      require(c>0, "Error parsing Consensus sequence from PDB ATOM line. Count <= 0 !?");
      counts.push_back(c);
    }
    
    totalCount = sum(counts);
}
