# RAPDF

Copyright by Sebastian Kelm, 2009, 2013

Based on the algorithms by Samudrala and Moult described here:

 *  __RAPDF__      http://dx.doi.org/10.1006/jmbi.1997.1479
 *  __Self-RAPDF__ http://dx.doi.org/10.1186/1472-6807-4-8

Contains a contact normalisation formula by
Sanne Abeln, 2007, University of Oxford, DPhil thesis, page 131

This program is a versatile C++ implementation of the protein 3D model quality assessment function RAPDF. The program includes options for creating RAPDF contact profiles, running in Self-RAPDF mode (where a group of models is scored using their own contact profiles), as well as additional more experimental features.

Results are not guaranteed to be identical to the published version of RAPDF, but should be similar.
