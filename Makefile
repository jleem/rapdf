CPP = g++
CPPFLAGS = -O2 -Wall
OFLAG = -o
.SUFFIXES :
%.o: %.cpp %.h skutil.h
	$(CPP) $(CPPFLAGS) -c $(OFLAG) $@ $<


all: \
	main \
	test_ContactMap \
	clashdet

scratch: \
	clean \
	all

main: pdb.o pdb.h contactmap.o contactmap.h scores.o scores.h main.o skutil.h
	$(CPP) $(CPPFLAGS) $(OFLAG)RAPDF pdb.o contactmap.o scores.o main.o

test_ContactMap: contactmap.o contactmap.h test_ContactMap.o skutil.h
	$(CPP) $(CPPFLAGS) $(OFLAG)test_ContactMap contactmap.o test_ContactMap.o

clashdet: skutil.h clashdet.o pdb.h pdb.o scores.h scores.o contactmap.h contactmap.o
	$(CPP) $(CPPFLAGS) $(OFLAG)clashdet clashdet.o pdb.o scores.o contactmap.o

clean:
	rm -f *.o RAPDFc test_ContactMap clashdet

