#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <sstream>

#include "skutil.h"
#include "pdb.h"
#include "scores.h"

using namespace std;


const double unit=0.1; // geometrical unit of 0.1 Angstroms


template<typename T>
class Matrix3D
{
  size_t dimX, dimY, dimZ;
  vector<T> data;
//   T *data;

  public:
    Matrix3D()
    : dimX(0), dimY(0), dimZ(0)
    {
    }
    
    Matrix3D(size_t x, size_t y, size_t z, T defVal)
    : dimX(x), dimY(y), dimZ(z), data(x*y*z, defVal)
    {
      if (x==0 or y==0 or z==0)
        throw OutOfBounds("Matrix3D::Matrix3D() : x,y,z dimensions must not be 0");
    }
    
/*
    Matrix3D(const Matrix3D& a)
    : dimX(a.dimX), dimY(a.dimY), dimZ(a.dimZ), data(a.data)
    {
    }
    
    &Matrix3D operator= (const Matrix3D& a)
    {
      dimX = a.dimX;
      dimY = a.dimY;
      dimZ = a.dimZ;
      data = a.data;
    }
*/

    inline size_t size() const
    {
      return data.size();
    }
    inline size_t X() const
    {
      return dimX;
    }
    inline size_t Y() const
    {
      return dimY;
    }
    inline size_t Z() const
    {
      return dimZ;
    }
    
    inline T& operator()(size_t x, size_t y, size_t z)
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(y));
      return data[x + dimX*y + dimX*dimY*z];
    }
    inline const T& operator()(size_t x, size_t y, size_t z) const
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(y));
      return data[x + dimX*y + dimX*dimY*z];
    }
    
    inline T& operator[](size_t i)
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
    inline const T& operator[](size_t i) const
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
};



class GeometricHash
{
  const Pdb* protein;
  Matrix3D<const Atom*> data;
  unsigned long dupFlag;
  map<size_t, pair<size_t, const Atom**> > dups;

//   vector<const Atom*> data;
  //map<int, Atom*> duplicates; // if several atoms occupy the same space, this is where we store the array pointer they will go.
  
  unsigned binWidth;
  
  // raw PDB data
  double minX, minY, minZ;
  double maxX, maxY, maxZ;

  double stringency;
  

  public:

    class StericClash : public ValueError
    {
      public:
        StericClash(const std::string& s)
        : ValueError(s)
        {
        }
    };

    
    GeometricHash(const Pdb& prot, double optimalDistance=3.5)
    : protein(&prot), dupFlag((unsigned long)&prot), binWidth((unsigned)(1+optimalDistance/unit/2)), minX(0), minY(0), minZ(0), maxX(0), maxY(0), maxZ(0), stringency(1.0)
    {
      init(prot);
      require(binWidth>0, "GeoometricHash : binWidth must be > 0 ! Please provide a positive optimalDistance parameter!");
      #ifdef DEBUG
      unsigned arraysCreated=0;
      unsigned arraysDeleted=0;
      #endif
      for (size_t i=0; i<prot.size(); i++)
      {
        size_t index = idx(prot[i]);
        //cerr << "Loading atom ";
        //prot[i].print(cerr);
        //cerr << "\n" << "Index: " << index << "\n";
        if (data[index] != NULL)
        {
          if ((unsigned long)data[index] != dupFlag)
          {
            const Atom** arr = new const Atom*[2];
            arr[0] = data[index];
            arr[1] = &(prot[i]);
            dups[index] = pair<size_t, const Atom**>((size_t)2, arr);
            data[index] = (const Atom*) dupFlag;
          }
          else
          {
            pair<size_t, const Atom**>& mydup = dups[index];
            const Atom** arr = new const Atom*[mydup.first+1];
            for (size_t j=0; j<mydup.first; j++)
              arr[j] = mydup.second[j];
            //memcpy(arr, mydup.second, sizeof(mydup.second));
            arr[mydup.first] = &(prot[i]);
            ++mydup.first;
            delete [] mydup.second;
            mydup.second = arr;
            #ifdef DEBUG
            arraysDeleted++;
            #endif
          }
          #ifdef DEBUG
          arraysCreated++;
          #endif
          //throw StericClash("Steric clash detected while assigning atoms to GeometricHash. Multiple atoms at same 3D Matrix index:\n"+toString(prot[i])+toString(*(data[index])));
        }
        else
        {
          data[index] = &(prot[i]);
        }
      }

      #ifdef DEBUG
      if (arraysCreated)
        cerr << "Arrays created: " << arraysCreated << "\n";
      if (arraysDeleted)
        cerr << "Arrays deleted: " << arraysDeleted << "\n";
      #endif
    }
   
    ~GeometricHash()
    {
      map<size_t, pair<size_t, const Atom**> >::iterator it = dups.begin();
      for (; it != dups.end(); ++it)
      {
        delete [] it->second.second;
      }
    }
    
    inline void setStringency(double d)
    {
      require(d>0.0 and d<=1.0, "Illigal stringency: "+toString(d));
      stringency = d;
    }


    inline unsigned hasClashes(double maxRadius=3.5, bool stopAtFirst=1)
    {
      Matrix3D<unsigned> sphere = distMatrix(maxRadius);
      
      unsigned clashes = 0;
      size_t size = protein->size();
      for (size_t i=0; i<size; i++)
      {
        clashes += doCube((*protein)[i], &GeometricHash::isClash, maxRadius, sphere, stopAtFirst);
        //clashes += doSphere((*protein)[i], &GeometricHash::isClash, maxRadius, sphere, stopAtFirst);
        if (stopAtFirst and clashes)
          return clashes;
      }
      return clashes / 2;
    }
    
    
  private:
    inline void init(const Pdb& prot)
    {
      if (prot.size() == 0)
        return;

      double miX, miY, miZ, maX, maY, maZ;
      miX = maX = prot[0].x;
      miY = maY = prot[0].y;
      miZ = maZ = prot[0].z;
      
      for (size_t i=1; i<prot.size(); i++)
      {
        const Atom& a = prot[i];
        if (a.x < miX)
          miX = a.x;
        if (a.x > maX)
          maX = a.x;
        if (a.y < miY)
          miY = a.y;
        if (a.y > maY)
          maY = a.y;
        if (a.z < miZ)
          miZ = a.z;
        if (a.z > maZ)
          maZ = a.z;
      }
      minX = miX;
      minY = miY;
      minZ = miZ;
      maxX = maX;
      maxY = maY;
      maxZ = maZ;
      size_t lenX = 1 + idxX(maX);
      size_t lenY = 1 + idxY(maY);
      size_t lenZ = 1 + idxZ(maZ);
      data = Matrix3D<const Atom*>(lenX, lenY, lenZ, NULL);
      #ifdef DEBUG
      //cerr << "minX: " << minX << "\n";
      //cerr << "minY: " << minY << "\n";
      //cerr << "minZ: " << minZ << "\n";
      //cerr << "maxX: " << maxX << "\n";
      //cerr << "maxY: " << maxY << "\n";
      //cerr << "maxZ: " << maxZ << "\n";
      cerr << "lenX: " << lenX << "\n";
      cerr << "lenY: " << lenY << "\n";
      cerr << "lenZ: " << lenZ << "\n";
      cerr << "data size: " << data.size() << "\n";
      #endif
    }

    inline size_t idxX(double x)
    {
      return (size_t)((x - minX) / (unit * binWidth));
    }
    inline size_t idxY(double y)
    {
      return (size_t)((y - minY) / (unit * binWidth));
    }
    inline size_t idxZ(double z)
    {
      return (size_t)((z - minZ) / (unit * binWidth));
    }
    inline size_t idx(size_t x, size_t y, size_t z)
    {
      return x + (data.X()*y) + (data.X()*data.Y()*z);
    }
    inline size_t idx(const Atom& a)
    {
      return (idxX(a.x)) + (data.X()*idxY(a.y)) + (data.X()*data.Y()*idxZ(a.z));
    }
    inline void idx(const Atom& a, int& x, int& y, int& z)
    {
      x = idxX(a.x);
      y = idxY(a.y);
      z = idxZ(a.z);
    }
    inline void idx(const Atom& a, size_t& x, size_t& y, size_t& z)
    {
      x = idxX(a.x);
      y = idxY(a.y);
      z = idxZ(a.z);
    }
    
    
    inline static double threshold(const Atom& a, const Atom& b)
    {
      return radius(a.atom[0]) + radius(b.atom[0]);
    }
    
    inline static double radius(char c)
    {
      switch(c)
      {
        case 'C': return 1.70;
        case 'N': return 1.55;
        case 'O': return 1.52;
        case 'S': return 1.80;
        case 'P': return 1.80;
        case 'H': return 1.20;
        default:
          throw ValueError("GeometricHash::radius() : Illegal atom type: "+c);
      }
    }
    

    inline static double lennard_jones(double vdw6, double dist, double well=100.0)
    {
      // vdw6 is the 6th power of the distance between the two atoms where the lennard-jones
      // potential is 0.
      // dist is the actual distance between the two atoms.
      // well is the depth of the potential well (a constant)
      return 4.0 * well * (dist/(pow(vdw6,2)) - dist/(vdw6));
    }
    

    inline static bool isClash(GeometricHash& self, const Atom& a, const Atom& b)
    {
      bool result = Scores::distance(a, b) < (self.stringency * threshold(a, b));
      if (result)
      {
        cerr << "Clash: distance="<< Scores::distance(a, b) << " cut-off=" << (self.stringency * threshold(a, b)) << "\n";
        a.print(cerr);
        b.print(cerr);
      }
      return result;
    }
    
    
    inline Matrix3D<unsigned> distMatrix(unsigned radius)
    {
      Matrix3D<unsigned> m(1+radius, 1+radius, 1+radius, 0);
      
      for(size_t dx=0; dx<=radius; dx++)
      {
        for(size_t dy=0; dy<=radius; dy++)
        {
          for(size_t dz=0; dz<=radius; dz++)
          {
            m(dx, dy, dz) = (unsigned)ceil(dist(dx, dy, dz));
          }
        }
      }

      return m;
    }

    
    unsigned doCube(const Atom& a, bool (*action)(GeometricHash&, const Atom&, const Atom&), double radius, const Matrix3D<unsigned>& distMat, bool stopOnSuccess=1)
    {
      long r = 1 + radius/(unit*binWidth);
      unsigned count=0;
      long x=idxX(a.x);
      long y=idxY(a.y);
      long z=idxZ(a.z);
      
      //cerr << "x,y,z = " <<x<<","<<y<<","<<z<<"\n";
      //cerr << "X,Y,Z = " <<data.X()<<","<<data.Y()<<","<<data.Z()<<"\n";
      
      long loX = max(x-r, (long)0);
      long loY = max(y-r, (long)0);
      long loZ = max(z-r, (long)0);
      long hiX = min(x+r, (long)(data.X()-1));
      long hiY = min(y+r, (long)(data.Y()-1));
      long hiZ = min(z+r, (long)(data.Z()-1));
      
      

      //cerr << "for(size_t ix="<<loX<<"; ix<="<<hiX<<"; ix++)\n";
      //cerr << "\tfor(size_t iy="<<loY<<"; iy<="<<hiY<<"; iy++)\n";
      //cerr << "\t\tfor(size_t iz="<<loZ<<"; iz<="<<hiZ<<"; iz++)\n";
      for(long ix=loX; ix<=hiX; ix++)
      {
        for(long iy=loY; iy<=hiY; iy++)
        {
          for(long iz=loZ; iz<=hiZ; iz++)
          {
            if (distMat(abs(ix-x), abs(iy-y), abs(iz-z)) <= r)
            {
              count += doStuff(ix, iy, iz, a, action, stopOnSuccess);
              if (count and stopOnSuccess)
                return count;
            }
          }
        }
      }
      
      return count;
    }
    
    
    // Iterates through a range, e.g. for(i=0; i<4; absinc(i)) ... like so: 0, 1, -1, 2, -2, 3, -3, 4
    inline void absinc(long& i)
    {
      if (i<=0)
        i=-i+1;
      else
        i = -i;
    }

    
    inline double dist(long dx, long dy, long dz)
    {
      return sqrt(pow(dx,2) + pow(dy,2) + pow(dz,2));
    }

    
    unsigned doSphere(const Atom& a, bool (*action)(GeometricHash&, const Atom&, const Atom&), double radius, Matrix3D<unsigned>& distMat, bool stopOnSuccess=1)
    {
      const static double root2 = sqrt(2.0);
      long maxR = 1 + (root2*radius)/(unit*binWidth);
      unsigned count=0;
      require(radius>0, "GeometricHash::doSphere() : radius <= 0");
      
      long x=idxX(a.x);
      long y=idxY(a.y);
      long z=idxZ(a.z);
      

      for (long r=0; r<=maxR; r++)
      {
        for (long dx=0; dx<=r; absinc(dx))
        {
          if (x+dx < 0 or (size_t)(x+dx) >= data.X())
            continue;
          for (long dy=0; dy<=abs(r)-abs(dx); absinc(dy))
          {
            if (y+dy < 0 or (size_t)(y+dy) >= data.Y())
              continue;
            
            long dz=r-abs(dx)-abs(dy);
            
            // This skips all cells not belonging to the sphere!
            //if (maxR < dist(dx, dy, dz))
            //  continue;
            if ((unsigned)maxR < distMat(abs(dx), abs(dy), dz))
              continue;
             
            if ((size_t)(z+dz) < data.Z())
            {
              count += doStuff(x+dx, y+dy, z+dz, a, action, stopOnSuccess);
              if (count and stopOnSuccess)
                return count;
            }

            if (0==dz)
              continue;
            
            if (z-dz >= 0)
            {
              count += doStuff(x+dx, y+dy, z-dz, a, action, stopOnSuccess);
              if (count and stopOnSuccess)
                return count;
            }
          }
        }
      }
      return count;
    }

    inline bool clashExcluded(const Atom& a, const Atom& b)
    {
      int d = a.resindex - b.resindex;
      if (!(abs(d) <= 1 and a.chain == b.chain))
        return 0;
      return 0 == d or (d < 0 and a.atom  == "C" and b.atom == "N") or (d > 0 and a.atom == "N" and b.atom == "C");
    }
    
    inline unsigned doStuff(long x, long y, long z, const Atom& a, bool (*action)(GeometricHash&, const Atom&, const Atom&), bool stopOnSuccess)
    {
      //cerr << "Checking index: " << x <<", "<< y <<", "<< z << "\n";
      const Atom* b;
      //try
      //{
        b = data(x, y, z);
      //}
      //catch(OutOfBounds)
      //{
      //  //cerr << "OutOfBounds\n";
      //  return 0;
      //}
      
      if (b == NULL)
      {
        //cerr << "NULL\n";
        return 0;
      }

      if ((unsigned long)b != dupFlag)
      {
        //cerr << (unsigned long)b << " != " << dupFlag << "\n";
        //cerr << "Singleton\n";
        if (!clashExcluded(a, *b) and action(*this, a, *b))
          return 1;
        else
          return 0;
      }
      else
      {
        //cerr << "Dup\n";
        unsigned count=0;
        pair<size_t, const Atom**>& dup = dups[idx(x, y, z)];
        //cerr << "First atom in bin: " << dup.first << " " << toString(*(dup.second));
        for (size_t i=0; i<dup.first; i++)
        {
          b = dup.second[i];
          if (!clashExcluded(a, *b) and action(*this, a, *b))
          {
            if (stopOnSuccess)
              return 1;
            else
              count++;
          }
        }
        return count;
      }
    }

};



int main(int argc, char* argv[])
{

    //vector<ContactMap> contacts;
    //
    //ContactMap cmap;
    //
    
  if (argc < 2)
  {
    cerr << "provide a PDB file as the first argument.\n";
    exit(1);
  }

  Pdb protein(argv[1]);

  //cerr << "Initializing geo...\n";
  
  // GeometricHash(protein, radius) :
  //  Radius is an optimisation parameter.
  //  It should reflect the maximum radius around each atom,
  //  which you expect to work at most of the time
  //  (e.g. for clash detection, use the
  //    vdW radius of carbon * 2).
  //
  GeometricHash geo(protein, 3.5);
  
  // Multiply all vdW radii with this number,
  // when doing clash detection.
  //
  geo.setStringency(0.65);
  
  //cerr << "Done initializing geo.\nNow checking for clashes...\n";
  
  // geo.hasClashes(radius, stopAtFirst);
  // Do clash detection. Count clashes.
  // Radius is the maximum radius (in Angstroms) around every atom to consider.
  // When stopAtFirst is true, the method will return when it finds the first clash.
  //  Otherwise, it will count the total number of clashes in the molecule.
  //
  unsigned clashes = geo.hasClashes(3.5, 0);

  printf("Total clashes:  %d\n", clashes);
}

