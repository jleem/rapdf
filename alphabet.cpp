/*
 *  alphabet.cpp
 *
 *  Created by Sebastian Kelm on 13/07/2009.
 *
 */

#include "alphabet.h"

using namespace std;


void Alphabet::transform(ContactMapIterator& in, ContactMap& out, const ResidueMap& mapping)
{
  while(in.next())
  {
    const string  key    = in.getKey();
    const Contact val    = in.getContact();
    
    string newkey = mapping[key];
    if (newkey.empty())
    {
      newkey = key;
    }
    
    out.add(newkey, val);
  }
}

ContactMap Alphabet::simplify(ContactMapIterator& in, const ResidueMap& mapping = Mappings::aa2type)
{
  ContactMap out(in.getIMin(), in.getIMax());
  transform(in, out, mapping);
  return out;
}

