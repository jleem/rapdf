/*
 *  pdb.h
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#ifndef PDB_H
#define PDB_H

#include <iostream>
#include <string>
#include <vector>



class Consensus
{
  public:
    int totalCount;
    std::vector<std::string> residues;
    std::vector<int> counts;
    
    Consensus(const std::string& line);
    
    inline bool empty() const
    {
      return 0 == totalCount;
    }
    
    inline void print(std::ostream& out = std::cout) const
    {
      out << totalCount << "\t" << toString(residues) << "\t" << toString(counts) << "\n";
    }
};



// Representation of a single ATOM in a PDB file
class Atom
{
  public:
    double x, y, z;
    int iatom, ires, resindex;
    std::string atom, res, chain, altloc, inscode;
    Consensus consensus;
    
//     Atom(double x, double y, double z, int iatom, std::string atom, int ires, std::string res, std::string chain, Consensus cons);
//     Atom(std::string x, std::string y, std::string z, std::string iatom, std::string atom, std::string ires, std::string res, std::string chain);
    Atom(const std::string& line);
    
    void print(std::ostream& out = std::cout) const;
    
    int operator<(const Atom& other) const;
    int operator>(const Atom& other) const;
};



class Pdb
{
  public:
    Pdb(char* name);
    Pdb(std::string name);
    Pdb(std::string name, std::istream& in);

    std::string getName() const {return name;}
    std::vector<Atom>& getAtoms() {return atoms;}
    
    inline unsigned int size() const {return atoms.size();}
    inline unsigned int countAtoms() const {return atoms.size();}
    unsigned int countResidues() const;
//     inline Atom& getAtom(int i) {return atoms[i];}
    
    Pdb copyByAtomType(std::string atype) const;
    void splitChains(std::vector<Pdb>& output, std::vector<std::string>& chainids) const;
    
    const Atom& operator[](int x) const;
    
    inline void print(std::ostream& out = std::cout) const
    {
      for (unsigned int i=0; i<atoms.size(); i++)
        atoms[i].print(out);
    }
    
    void sort();

  private:
    std::string name;
    std::vector<Atom> atoms;
    unsigned int residueCount;
    
    void initAtoms(std::istream& in);
    
    Pdb(std::string name, std::vector<Atom>& a, int resCount=-1);
};


inline std::string toString(Atom a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}
inline std::string toString(Pdb a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}


#endif
