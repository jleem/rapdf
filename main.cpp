#include <iostream>    // manipulate console
#include <fstream>     // manipulate files
#include <string>      // standard data structure
#include <vector>      // standard data structure

#include <cstdlib>     // standard C library

#include "skutil.h"     // my often-used utility functions

#include "pdb.h"       // parsing PDB files
#include "contactmap.h"// calculating atom-atom contact maps
#include "scores.h"    // calculating MQAP scores

using namespace std;


const string cmap_ext = ".cmap";


inline ContactMap readCMap(const char* filepath)
{
    try
    {
      return unserializeContactMap(filepath);
    }
    catch (RequirementError e)
    {
      cerr << "ERROR: Could not read input file '" << filepath << "'\n";
      exit(2);
    }
}

inline void writeCMap(const char* filename, ContactMap cmap)
{
    ofstream out(filename);
    require(out, "ERROR: Could not write contact map to file. Check permissions.");
    serializeContactMap(cmap, out);
    out.close();
}

inline double score(const string& filename, const ContactMap& cmap, const ContactMap& db, double add, int normalise)
{
    // Calculate final RAPDF scores for input files
    //cerr << "Scoring protein '" << filename << "'\n";
    ContactMap::Iterator it = cmap.getIterator();
    double s = Scores::RAPDF::score(it, db, add, normalise);
    cout << basename(filename) << "\t " << s << "\n";
    return s;
}
inline double score(const string& filename, ContactMapReader& cread, const ContactMap& db, double add, int normalise)
{
    // Calculate final RAPDF scores for input files
    //cerr << "Scoring protein '" << filename << "'\n";
    double s = Scores::RAPDF::score(cread, db, add, normalise);
    cout << basename(filename) << "\t " << s << "\n";
    return s;
}

void writeDensities(const vector<double> densities, const vector<string> filenames, const char* outfile)
{
  require(densities.size() == filenames.size(), "ERROR: writeDensities : densities.size() != filenames.size()");
  
  ofstream out(outfile);
  if (!out)
  {
    cerr << "ERROR: Could not write densities to file. Check permissions.";
    return;
  }
  for(unsigned int i=0; i<densities.size(); i++)
  {
    out << densities[i] << "\t" << basename(filenames[i]) << "\n"; 
  }
  out.close();
}

vector<double> readDensities(const vector<string> filenames, const char* infile)
{
  ifstream in(infile);
  require(in, "ERROR: readDensities : Could not read density file.");
  
  map<string, double> f2d;
  string line;
  while(getline(in, line))
  {
    istringstream tokens(line);
    string token;

    tokens >> token;
    string f = trim(token);
    double d = convertTo<double>(line.substr(token.size()+1));
    f2d[f] = d;
  }

  vector<double> densities(filenames.size(), 0.0);
  for(unsigned int i=0; i<filenames.size(); i++)
  {
    string bname = basename(filenames[i]);
    require(f2d.find(bname)!=f2d.end(), string("ERROR: Density file contains no information about structure: ")+bname);
    densities[i] = f2d[bname];
  }
  in.close();

  return densities;
}


//
// MAIN CODE EXECUTION
//

int main(int argc, char* argv[])
{
    // Options
    bool optSelf           = false; // s | expect decoys to be of same protein. calculate densities and a weighted database
    
    bool optReadDensities  = false; // x | read densities from specified file
    bool optWriteDensities = false; // y | write densities to specified file
    bool optDensityFactor  = false; // w | option "w" is provided
    double densityWeight   = 4.5;   // w | weight factor for density scoring (optSelf must be given)
    
    bool optConsensus      = false; // o | use consensus information at the end of ATOM lines (PDB files made by the "consensus" programme)
    bool optNormConsensus  = false; // o | normalise consensus info, such that conserved residues are not over-represented
    
    bool optContactMode    = false; // c | option "c" is provided
    char contactMode       = 'a';   // c | contact mode: 'a' for all-vs-all, 'c' for intra-chain, 'd' for docking mode (inter-chain)
    
    bool optReadMaps       = false; // m | arguments are contact maps, not protein structures
    bool optResume         = false; // n | arguments are structures, but if a map exists, us that instead
    bool optRemoveMaps     = false; // r | remove (or do not create) cmap files
    
    bool optReadDb         = false; // d | contact map database provided - use RAPDF, not self-RAPDF
    bool optWriteDb        = false; // e | write out contact maps to files
    
    bool optAddDbScore     = false; // a | add something to every database score
    bool optNormScore      = false; // l | option "l" is provided
    int scoreNormMode      = 0;     // l | normalise score
    bool optNoScore        = false; // z | disable scoring
   
    double addToDb         = 0.0;
   
    // Where to write the contact database (if applicable)
    char* dbFileName = 0;
    char* densityFileName = 0;
    
    
    {
      // Command line option parsing
      
      int c;
      while (-1 != (c = getopt (argc, argv, "hc:opmnrd:e:a:l:zsw:x:y:")))
      {
          switch (c)
          {
            case 'h':
            {
              cerr << "RAPDF model quality assessment function\n"
                      "\n"
                      "      Copyright by Sebastian Kelm, 2009, 2013.\n"
                      "\n"
                      "      Original algorithm by Samudrala and Moult described here:\n"
                      "        RAPDF       http://dx.doi.org/10.1006/jmbi.1997.1479\n"
                      "        Self-RAPDF  http://dx.doi.org/10.1186/1472-6807-4-8\n"
                      "      Contact normalisation formula by Sanne Abeln, 2007,\n"
                      "        University of Oxford, DPhil thesis, page 131 (eq. 5.20).\n"
                      "\n"
                      "\n"
                      "USAGE:\n"
                      "\n"
                      "      " << basename(argv[0]) << " [OPTIONS] FILES...\n"
                      "\n"
                      "      FILES are pdb structures by default.\n"
                      "      FILES are contact maps, if the -m option is enabled.\n"
                      "\n"
                      "OPTIONS:\n"
                      "\n"
                      "      Options not divided by empty lines are logical groups and (mostly) mutually exclusive.\n"
                      "      If you are unsure certain combinations are legal, just try them, without specifying input\n"
                      "      files. Illegal combinations of options will result in an ARGUMENT ERROR message.\n"
                      "\n"
                      "      GENERAL OPTIONS:\n"
                      "\n"
                      "        -h        Show this help message and exit.\n"
                      "\n"
                      "        -o        Use consensus sequence information found at the end of PDB ATOM lines.\n"
                      "        -p        Normalise consensus information to avoid over-representing conserved residues.\n"
                      "\n"
                      "        -c M      Set contact mode to M, where M is one of the following characters:\n"
                      "                    a    All-vs-All (default)\n"
                      "                    c    Within-Chain only\n"
                      "                    d    Across-Chains only (docking mode)\n"
                      "\n"
                      "        -x FILE   Read weights corresponsing to each input structure from FILE.\n"
                      "                  Every line in FILE should look something like this:\n"
                      "                  2porA.pdb        0.7682\n"
                      "\n"
                      "        -m        Read in contact maps, not structures.\n"
                      "        -n        RESUME MODE: Read in structures, but if a cmap is present, use that instead.\n"
                      "        -r        Remove (clean up) .cmap files at the end of execution.\n"
                      "\n"
                      "        -d FILE   Read contact map database from FILE, instead of\n"
                      "                  generating it from the input files.\n"
                      "        -e FILE   Write contact map database to FILE.\n"
                      "\n"
                      "        -a NUM    While scoring, add NUM (a floating point number) to each score\n"
                      "                  retrieved from the database. This has no effect on any output\n"
                      "                  files - it only affects the scores printed to STDOUT.\n"
                      "        -l M      Normalise score to make it protein-length-independent. Legal values for M:\n"
                      "                     0    (default) Divide by the expected number of contacts in a globular\n"
                      "                          protein of the input protein's length.\n"
                      "                    -1    Divide by the number of contacts actually scored by the database.\n"
                      "                    -2    Divide by the total number of contacts in the decoy (scored or not).\n"
                      "                    ... or any positive integer corresponging to the length of a protein.\n"
                      "        -z        Disable scoring (useful when only writing files).\n"
                      "\n"
                      "\n"
                      "      SELF-RAPDF MODE:\n"
                      "\n"
                      "        NOTE:\n"
                      "             SELF-RAPDF mode requires all input structures to be\n"
                      "             decoys of the same protein, i.e. the same sequence.\n"
                      "             This programme does not align the decoys. You need to\n"
                      "             Pre-align them using an external structure alignment\n"
                      "             programme, e.g. MAMMOTH.\n"
                      "             By default, this mode will read in structures and compute\n"
                      "             density scores, which are then used as weights to build\n"
                      "             a weighted database. Structures are then scored.\n"
                      "\n"
                      "        -s        Enable SELF-RAPDF mode.\n"
                      "\n"
                      "        -w NUM    Set the weight factor for the density score to NUM.\n"
                      "        -y FILE   Write densities to FILE.\n"
                      "\n";
              return 1;
            }
            case 's':
            {
//               cerr << "SET OPTION: [s] RAPDF (NoSelf) mode.\n";
//               optNoSelf = true;
              cerr << "SET OPTION: [" << (char)c << "] self-RAPDF mode.\n";
              optSelf = true;
              break;
            }
            case 'c':
            {
              cerr << "SET OPTION: [" << (char)c << "] Set contact mode to: " << optarg << "\n";
              require(string(optarg).size() == 1, "ARGUMENT ERROR: Option -c takes a single character as argument!");
              optContactMode = true;
              contactMode = optarg[0];
              require(contactMode == 'a' or contactMode == 'c' or contactMode == 'd', "ARGUMENT ERROR: Option -c takes one of the following chars as argument: 'a', 'c', 'd'.");
              break;
            }
            case 'o':
            {
              cerr << "SET OPTION: [" << (char)c << "] Use consensus sequence information in PDB ATOM lines.\n";
              optConsensus = true;
              break;
            }
            case 'p':
            {
              cerr << "SET OPTION: [" << (char)c << "] Normalise consensus information.\n";
              optNormConsensus = true;
              break;
            }
            case 'm':
            {
              cerr << "SET OPTION: [" << (char)c << "] Input files are contact maps.\n";
              optReadMaps = true;
              break;
            }
            case 'n':
            {
              cerr << "SET OPTION: [" << (char)c << "] RESUME MODE: Input files are structures, but if cmap is present, use that instead.\n";
              optResume = true;
              break;
            }
            case 'r':
            {
              cerr << "SET OPTION: [" << (char)c << "] Remove .cmap files that were created during execution.\n";
              optRemoveMaps = true;
              break;
            }
            case 'd':
            {
              cerr << "SET OPTION: [" << (char)c << "] Read contact database from file '" << optarg << "'\n";
              optReadDb  = true;
              dbFileName = optarg;
              break;
            }
            case 'e':
            {
              cerr << "SET OPTION: [" << (char)c << "] Write contact database to file.\n";
              optWriteDb = true;
              dbFileName = optarg;
              break;
            }
            case 'x':
            {
              cerr << "SET OPTION: [" << (char)c << "] Read densities from file '" << optarg << "'\n";
              optReadDensities = true;
              densityFileName = optarg;
              break;
            }
            case 'y':
            {
              cerr << "SET OPTION: [" << (char)c << "] Write densities to file.\n";
              optWriteDensities = true;
              densityFileName = optarg;
              break;
            }
            case 'w':
            {
              optDensityFactor = true;
              densityWeight = atof(optarg);
              cerr << "SET OPTION: [" << (char)c << "] Density weight factor = " << densityWeight << "\n";
              require(densityWeight > 0, "ERROR: Density weight factor must be > 0.");
              break;
            }
            case 'a':
            {
              optAddDbScore = true;
              addToDb = atof(optarg);
              cerr << "SET OPTION: [" << (char)c << "] Add score to all DB fields : " << addToDb << "\n";
              require(addToDb > 0, "ERROR: Score added to all database fields must be > 0.");
              break;
            }
            case 'l':
            {
              optNormScore = true;
              scoreNormMode = atoi(optarg);
              cerr << "SET OPTION: [" << (char)c << "] Normalise score. Normalisation mode (-) or protein length (+) : " << scoreNormMode << "\n";
              break;
            }
            case 'z':
            {
              cerr << "SET OPTION: [" << (char)c << "] Disable scoring.\n";
              optNoScore = true;
              break;
            }
            case '?':
            {
              if (optopt == 'd' or optopt == 'w' or optopt == 'w')
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
              else if (isprint (optopt))
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
              else
                fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
              return 1;
            }
            default:
              abort();
          }
      }
      
      // End of command line option parsing
    }
    
    // Some restrictions on argument combinations
    // 
    require(!(optReadDensities and optWriteDensities), "ARGUMENT ERROR: Mutually exclusive options: -x, -y");
    require(!(optReadMaps      and optConsensus),      "ARGUMENT ERROR: Mutually exclusive options: -m, -o");
    require(!(optReadMaps      and optRemoveMaps),     "ARGUMENT ERROR: Mutually exclusive options: -m, -r");
    require(!(optReadMaps      and optResume),         "ARGUMENT ERROR: Mutually exclusive options: -m, -n");
    require(!(optResume        and optRemoveMaps),     "ARGUMENT ERROR: Mutually exclusive options: -n, -r");
    require(!(optReadMaps      and optContactMode),    "ARGUMENT ERROR: Mutually exclusive options: -m, -c");
    require(!(optReadDb        and optWriteDb),        "ARGUMENT ERROR: Mutually exclusive options: -d, -e");
    require(!(optSelf          and optReadDb),         "ARGUMENT ERROR: Mutually exclusive options: -s, -d");
    require(!(optReadDensities and optDensityFactor),  "ARGUMENT ERROR: Mutually exclusive options: -x, -w");
    require(!(optAddDbScore    and optNoScore),        "ARGUMENT ERROR: Mutually exclusive options: -a, -z");
    require(!(optNormScore     and optNoScore),        "ARGUMENT ERROR: Mutually exclusive options: -l, -z");
    //require(!(!optSelf         and optReadDensities),  "ARGUMENT ERROR: Option -x illegal without option -s");
    require(!(!optSelf         and optWriteDensities), "ARGUMENT ERROR: Option -y illegal without option -s");
    require(!(!optSelf         and optDensityFactor),  "ARGUMENT ERROR: Option -w illegal without option -s");
    require(!(optSelf          and optReadMaps and !optReadDensities), "ARGUMENT ERROR: Supplying options -s and -m requires option -x as well.");
    require(!(!optConsensus    and optNormConsensus),  "ARGUMENT ERROR: Option -p illegal without option -o");
    
    
    // Storage for atom-atom contact database
    ContactMap  contactDb;
    
    // Storage for accepted input filenames
    vector<string> structures; // only filled if input consists of structures
    vector<string> cmaps;      // always filled
    
    
    if (optReadDb)
    {
      cerr << "Parsing contact database...\n";
      contactDb = readCMap(dbFileName);
      require(!contactDb.isEmpty(), "Contact database is empty!");
//       cerr << toString(contactDb["ALA C ALA C"].asVector()) << "\n";
    }
    
    
    cerr << "Parsing input files...\n";
    for (int i=optind; i<argc; i++)
    {
      string filepath = string(argv[i]);
      
      if (!isFileReadable(filepath))
      {
        cerr << "ERROR: File not readable. Skipping: '" << filepath << "'\n";
        continue;
      }

      if (optReadMaps)
      {
        cmaps.push_back(filepath);
      }
      else
      {
        cmaps.push_back(basename(filepath+"."+contactMode+cmap_ext));
        structures.push_back(filepath);
      }
    }
    
    
    if (cmaps.empty())
    {
      cerr << "ERROR: No input files. For usage info, type\n\t" << basename(argv[0]) << " -h\n";
      return 1;
    }
   
    vector<Pdb> proteinsCA;
    
    if (!optReadMaps)
    {
      cerr << "Building contact maps...\n";
      require(cmaps.size() == structures.size(), "BUG: cmaps.size() != structures.size()");
      
      // Write cmap files, as they are not given as input
      for(unsigned int i=0; i<structures.size(); i++)
      {
        if (optResume and isFileReadable(cmaps[i]))
        {
          cerr << "Existing cmap found for protein '" << structures[i] << "'.\n";
          
          if (optSelf) // need to calculate densities later
          {
            Pdb protein(structures[i]);
            proteinsCA.push_back(protein.copyByAtomType("CA"));   // BRANCH 1: self   + noDB
          }
        }
        else
        {
          ContactMap cmap;
          //cerr << "Now loading protein '" << structures[i] << "'...\n";
          Pdb protein(structures[i]);
          cerr << "Building cmap for protein '" << structures[i] << "'...\n";
          Scores::RAPDF::buildContactMap(protein, cmap, contactMode, optConsensus, optNormConsensus);
          
          if (optSelf) // need to calculate densities later
            proteinsCA.push_back(protein.copyByAtomType("CA"));   // BRANCH 1: self   + noDB
          else if (!optResume)
          {
            if (!optReadDb) // need to build a database
              contactDb.add(cmap);                                  // BRANCH 2: noSelf + noDB
            else if (!optNoScore)
            {
              score(structures[i], cmap, contactDb, addToDb, scoreNormMode);      // BRANCH 3: noSelf + DB
              if (optRemoveMaps)
                continue; // don't write cmap to file
            }
          }
          writeCMap(cmaps[i].c_str(), cmap);
        }
        //
        // While we've got a cmap object, do the next step already, to save time
        // 
      }
      
      if (!optResume and !optSelf and optReadDb)
        return 0;                  // BRANCH 3 is DONE!
    }
    

    vector<double> densities;
    
    if (!proteinsCA.empty())
    {
      densities = Scores::RAPDF::density(proteinsCA);
      densities = Scores::RAPDF::normaliseDensity(densities, densityWeight);
      
      if (1==densities.size())
      {
        densities[0] = 1.0;
      }
      
      cerr << "Density Scores: " << toString(densities) << "\n";
      
      if (optWriteDensities)
      {
        writeDensities(densities, structures, densityFileName);
      }
    }
    else if (optReadDensities)
    {
      densities = readDensities(structures, densityFileName);
    }

    if (contactDb.isEmpty())
    {
      cerr << "Creating (weighted) contact database from input structures...\n";
      for(unsigned int i=0; i<cmaps.size(); i++)
      {
        double density = 1.0;
        if (!densities.empty())
          density = densities[i];
        
        ContactMapReader cread = ContactMapReader(cmaps[i].c_str());
        while(cread.next())
        {
          string key = cread.getKey();
          Contact c  = cread.getContact();
//          cerr << key << " : " << toString(c) << "\n";
          contactDb.add(key, c, density);
        }
      }
    }
    
    require(!contactDb.isEmpty(), "ERROR: Creating contact database failed. No contacts detected?");
      
    
    //
    // Write out the database of contacts to a file
    //
    if (optWriteDb)
    {
      cerr << "Writing database file...\n";
      writeCMap(dbFileName, contactDb);
    }
    

    //
    // Calculate scores
    //
    if (!optNoScore)
    {
      cerr << "Calculating scores...\n";
      for (unsigned int i=0; i<cmaps.size(); i++)
      {
        ContactMapReader cread = ContactMapReader(cmaps[i].c_str());
        score(cmaps[i], cread, contactDb, addToDb, scoreNormMode);
      }
    }
    
    if (optRemoveMaps)
    {
      for(unsigned int i=0; i<cmaps.size(); i++)
      {
        require(remove(cmaps[i].c_str()) == 0, "ERROR: Could not clean up cmap file. Check permissions ");
      }
    }

    return 0; // the end
}
